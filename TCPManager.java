/**
 * TCPManager.java
 * @author Alexander R. Cavaliere <arc6393@rit>
 */

import java.net.Socket;
import java.net.ServerSocket;
import java.io.IOException;

/**
 * TCPManager
 *
 * Listens for incoming TCP Connections to the Server and spawns
 * TCPReaders for each of the connections. 
 */
public class TCPManager extends Thread
{
    // Private Class State
    private ServerSocket serverTCPSocket;
    private Server timeServer;

    /**
     * TCPManager
     *
     * Public Constructor
     * @param serverTCPSocket ServerSocket
     * @param timeServer Server
     */
    public TCPManager( ServerSocket serverTCPSocket, Server timeServer )
    {
        this.serverTCPSocket = serverTCPSocket;
        this.timeServer = timeServer;
    }

    /**
     * run
     *
     * Listens for incoming TCP connections and spawns TCPServerReader threads
     * to handle the incoming client request.
     */
    public void run()
    {
        try
            {
                for( ; ; )
                    {
                        Socket socket = serverTCPSocket.accept();
                        TCPServerReader reader = new TCPServerReader( socket, timeServer );
                        reader.start();
                    }
            }
        catch( IOException ioe )
            {
                System.err.println( ioe );
            }
        finally
            {
                try
                    {
                        serverTCPSocket.close();
                    }
                catch( IOException ioe )
                    {
                        System.err.println( ioe );
                        System.exit( 1 );
                    }
            }
                    
    }
}
