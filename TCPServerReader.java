/**
 * TCPServerReader.java
 * @author Alexander R. Cavaliere <arc6393@rit>
 */

import java.io.IOException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;

import java.net.ServerSocket;
import java.net.Socket;
import java.net.InetSocketAddress;

import java.nio.charset.Charset;

/**
 * TCPServerReader
 *
 * Handles a single TCP connection
 */
public class TCPServerReader extends Thread
{

    // Class State
    private Server timeServer;
    private Socket socket;
    
    private DataInputStream in;
    private DataOutputStream out;

    private static final int MAXIMUM_MESSAGE_SIZE = 204;
    private static final int MAX_LENGTH_USERNAME_PASSWORD = 8;

    /**
     * TCPServerReader
     *
     * Public Constructor
     * @param socket Socket
     * @param timeServer Server
     */
    public TCPServerReader( Socket socket, Server timeServer )
    {
        try
            {
                this.socket = socket;
                socket.setTcpNoDelay( true );
            }
        catch( IOException ioe )
            {
                System.err.println( ioe );
                System.exit( 1 );
            }

        this.timeServer = timeServer;
    }

    /**
     * run
     *
     * Read in the request from the connecting client
     */
    public void run()
    {
        byte b;
        
        String username = "";
        String password = "";
        
        int timeStatus;
        long time;

        try
            {
                for( ; ; )
                    {
                        out = new DataOutputStream( socket.getOutputStream() );
                        in = new DataInputStream( socket.getInputStream() );
                        
                        b = in.readByte();

                        System.out.printf( "Received TCP Packet from %s\n\n", socket.getRemoteSocketAddress().toString() );

                        switch( b )
                            {
                            case 'Q':
                                out.writeByte( 'T' ); // Time response message "header"
                                out.writeLong( timeServer.getTime() );
                                out.flush();
                                break;
                            case 'S':
                                time = in.readLong();

                                short usernameLength = in.readShort();
                                for( short position = 0; position < usernameLength; ++position )
                                        username += (char)in.readByte();

                                short passwordLength = in.readShort();
                                for( short position = 0; position < passwordLength; ++position )
                                    password += (char)in.readByte();

                                timeStatus = timeServer.setTime( username, password,
                                                                 time, socket.getRemoteSocketAddress() );

                                // Clear the username and password for the next request
                                username = "";
                                password = "";
                                    
                                if( timeStatus == 0 )
                                    out.writeByte( 'T' ); // ACK Time Change
                                else
                                    out.writeByte( 'N' ); // NAK Time Change

                                out.writeLong( timeServer.getTime() );
                                out.flush();
                                break;
                            default:
                                System.err.println( "BAD MESSAGE" );
                                throw new IOException( "Bad Message Sent to Time Server" );
                            }
                    }// End For
            }
        catch( IOException ioe )
            {
            }
        finally
            {
                try
                    {
                        socket.close();
                    }
                catch( IOException closeFailure )
                    {
                        System.err.println( closeFailure );
                        System.exit( 1 );
                    }
            }
    }// End Run
}
    
