/**
 * tsapp.java
 * Author: Alexander R. Cavaliere <arc6393@rit.edu>
 * Date: 2017-02-21
 */

import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.DatagramSocket;
import java.net.Socket;
import java.io.IOException;

public class tsapp
{
    private static int CLIENT_CODE = 1;
    private static int SERVER_CODE = 2;

    public static void main( String[] args )
    {
        // Command Line Argument variables

        // Defaults
        boolean useUDP = true;
        boolean clientProtocolFlagSet = false;
        boolean useCalendarFormat = false;
        int queryAmount = 1;

        // Additional Values
        String username = "";
        String password = "";
        int time = -1; // Must be explicitly set
        
        // Required Values
        int server = 0; // 0 -> Not set, 1 -> Client, 2 -> Server
        String host = ""; // For server & client
        int serverPort = -1; // For client
        // For Server Only
        int udpPort = -1; 
        int tcpPort = -1;

        // Parse the command line arguments
        for( int position = 0; position < args.length; ++position )
            {
                String command = args[position];
                try
                    {
                        switch( command )
                            {
                            case "-s": // Server flag (Required)
                                if( server == CLIENT_CODE )
                                    {
                                        System.err.println( "Either -s or -c may be flagged; please use only one!" );
                                        System.exit( 1 );
                                    }
                                server = SERVER_CODE;
                                if( args[position+1].matches( "(\\W{1,2}[A-z]$)" ) )
                                    host = "0.0.0.0";
                                else
                                    host = args[++position];
                                break;
                            case "-c": // Client flag (Required)
                                if( server == SERVER_CODE )
                                    {
                                        System.err.println( "Either -s or -c may be flagged; please use only one!" );
                                        System.exit( 1 );
                                    }
                                server = CLIENT_CODE;
                                if( args[position+1].matches( "(\\W{1,2}[A-z]$)" ) )
                                    {
                                        System.err.println( "Missing hostname for -c argument" );
                                        System.exit( 1 );
                                    }
                                host = args[++position];
                                break;
                            case "-u": // Use UDP (default)
                                if( server == SERVER_CODE )
                                    {
                                        System.err.println( "-u flag is for client mode only; please remove flag and try again!" );
                                        System.exit( 1 );
                                    }
                                if( clientProtocolFlagSet )
                                    {
                                        System.err.println( "Please use either the -u flag or the -t flag; not both!");
                                        System.exit( 1 );
                                    }
                                useUDP = true;
                                clientProtocolFlagSet = true;
                                break;
                            case "-t": // Use TCP
                                if( server == SERVER_CODE )
                                    {
                                        System.err.println( "-t flag is for client mode only; please remove flag and try again!" );
                                        System.exit( 1 );
                                    }
                                if( clientProtocolFlagSet )
                                    {
                                        System.err.println( "Please use either the -u flag or the -t flag; not both!");
                                        System.exit( 1 );
                                    }
                                useUDP = false;
                                clientProtocolFlagSet = true;
                                break;
                            case "-z": // Use UTC; otherwise Calendar format
                                if( server == SERVER_CODE )
                                    {
                                        System.err.println( "-z flag is for client mode only; please remove flag and try again!" );
                                        System.exit( 1 );
                                    }
                                useCalendarFormat = true;
                                break;
                            case "-T": // Set Server Time
                                if( args[position+1].matches( "(\\W{1,2}[A-z]$)" ) )
                                    {
                                        System.err.println( "Missing time number for -T flag" );
                                        System.exit( 1 );
                                    }
                                time = Integer.parseInt( args[++position] );
                                break;
                            case "--user": // Provided Username
                                if( args[position+1].matches( "(\\W{1,2}[A-z]$)" ) )
                                    {
                                        System.err.println( "Missing username for the --user flag" );
                                        System.exit( 1 );
                                    }
                                username = args[++position];
                                break;
                            case "--pass": // Provided Password
                                if( args[position+1].matches( "(\\W{1,2}[A-z]$)" ) )
                                    {
                                        System.err.println( "Missing password for the --pass flag" );
                                        System.exit( 1 );
                                    }
                                password = args[++position];
                                break;
                            case "-n": // Number of times to query the server
                                if( args[position+1].matches( "(\\W{1,2}[A-z]$)" ) )
                                    {
                                        System.err.println( "Missing query repeat time for -n flag" );
                                        System.exit( 1 );
                                    }
                                queryAmount = Integer.parseInt( args[++position] );
                                break;
                            default:
                                break; // Invalid Command 
                            }
                        // If we are a server get the ports
                        if( server == SERVER_CODE && position == args.length - 2 )
                            {
                                udpPort = Integer.parseInt( args[position] );
                                tcpPort = Integer.parseInt( args[++position] );
                            }
                        else if( server == SERVER_CODE && position == args.length - 1 && udpPort == -1 )
                            {
                                udpPort = Integer.parseInt( args[position] );
                            }
                        // If we a client get the server's port
                        else if( server == CLIENT_CODE && position == args.length - 1 )
                            {
                                if( serverPort == -1 ) // Make sure that we haven't already set the value
                                    serverPort = Integer.parseInt( args[position] );
                            }
                    }
                catch( NumberFormatException nfe )
                    {
                        // We don't really need to worry about this
                    }
            } // Done Parsing Command Line Arguments

        // If we don't get port to work with as a server we can't run!
        if( udpPort == -1 && server == SERVER_CODE )
            {
                System.out.printf( "You must set a port for UDP usage! \n\n" );
                System.exit( 1 );
            }
        
        Server serverProgram = null;
        if( server == SERVER_CODE ) // Start the UDP and TCP Readers and let the clients begin their connections!
            {
                System.out.printf( "Starting as a Server\n\n" );
                serverProgram = new Server( host, udpPort, tcpPort, time, username, password );
                if( udpPort != -1 )
                    serverProgram.createUDPReader( serverProgram.getUDPSocket() );
                if( tcpPort != -1 )
                    serverProgram.createTCPReader( serverProgram.getTCPSocket() ); 
            }

        Client clientProgram = null;
        if( server == CLIENT_CODE ) // Create the client with the passed in flags
            {
                System.out.printf( "Starting as a Client \n\n" );
                clientProgram = new Client( host, serverPort, useUDP, useCalendarFormat, queryAmount, time, username, password );
                if( time == -1 )
                    clientProgram.getTime();
                else
                    clientProgram.setTime();
            }
    }// end main
}// end tsapp
