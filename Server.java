/** Server.java
 * @author Alexander R. Cavaliere <arc6393@rit.edu>
 */

import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.DatagramSocket;
import java.net.Socket;
import java.net.SocketAddress;

import java.io.IOException;
import java.nio.charset.Charset;

public class Server
{
    // Class State
    private String username;
    private String password;
    private long time;
    
    private String host;
    private int udpPort;

    private int tcpPort;

    private DatagramSocket udpSocket;
    private ServerSocket tcpSocket;

    private UDPServerReader udpReader;
    private TCPManager tcpManager;

    private static final String CHARACTER_SET_DEFAULT = "US-ASCII";

    /**
     * Server
     *
     * Public Constructor
     * @param host - String - The hostname of the server
     * @param udpPort - int - The port the server will use for UDP 
     * @param tcpPort - int - The port the server will use for TCP
     * @param time - int - The starting time for the server
     * @param username - String - The username to login and change the time
     * @param password - String - The password to login and change the time
     */
    public Server( String host, int udpPort, int tcpPort, int time, String username, String password )
    {
        // Set the variables
        // Unset variables will be -1 (numeric) or "" (Strings)
        this.host = host;
        this.udpPort = udpPort;
        this.tcpPort = tcpPort;
        this.time = (long)time;

        this.username = username;
        this.password = password;
        
        // Sockets
        tcpSocket = null;
        udpSocket = null;
        
        // Attempt to create and bind sockets
	try
	    {
                if( tcpPort != -1 )
                    {
                        tcpSocket = new ServerSocket();
                        tcpSocket.bind( new InetSocketAddress( host, tcpPort ) );
                    }
                if( udpPort != -1 )
                    udpSocket = new DatagramSocket( new InetSocketAddress( host, udpPort ) );
	    }
	catch( IOException ioe )
	    {
		System.out.println( "Socket failed to be created" );
		System.out.println( ioe );
		System.exit( 1 );
	    }

        System.out.printf( "Starting Server at %d\n\n", this.time );

        if( udpPort != -1 )
            System.out.printf( "Listening for UDP on Port: %d\n\n", udpPort );
        if( tcpPort != -1 )
            System.out.printf( "Listening for TCP on Port: %d\n\n", tcpPort );

        if( !username.equals( "" )  && !password.equals( "" ) )
            {
                System.out.printf( "Username set to %s\n\n", username );
                System.out.printf( "Password set to %s\n\n", password );
            }
        else
            System.out.printf( "Username and Password unset; Server Time Cannot Be Changed\n\n" );
        
    }

    /**
     * getUDPSocket
     *
     * Returns the DatagramSocket associated with the Server
     * @return udpSocket
     */
    public DatagramSocket getUDPSocket()
    {
        return this.udpSocket;
    }

    /**
     * getTCPSocket
     *
     * Returns the ServerSocket associated with the Server
     * @return tcpSocket
     */
    public ServerSocket getTCPSocket()
    {
        return this.tcpSocket;
    }
    
    /**
     * createUDPReader
     *
     * Gets the UDP listening thread up and running
     * @param udpSocket DatagramSocket The Server's DatagramSocket
     */
    public void createUDPReader( DatagramSocket udpSocket )
    {
        udpReader = new UDPServerReader( udpSocket, this );
        udpReader.start();
    }
    
    /**
     * createTCPReader
     *
     * Starts a thread which will listening for incoming TCP connections
     * and spawn a thread each time a client connects via TCP.
     * @param tcpSocket ServerSocket The Server's ServerSocket
     */
    public void createTCPReader( ServerSocket tcpSocket )
    {
        tcpManager = new TCPManager( tcpSocket, this );
        tcpManager.start();
    }
    
    /**
     * getTime
     *
     * Returns the time
     * @return time long
     */
    public synchronized long getTime()
    {
        return this.time;
    }

    /**
     * setTime
     *
     * Checks that the client has provided the correct username and password before changing the Server's
     * time. Otherwise this message will return that a change did not happen.
     * @param username String
     * @param password String
     * @param time long
     * @param requestor SocketAddress
     * @return 0 Succeeded at changing time
     * @return 1 failed to provide correct credentials
     */
    public synchronized int setTime( String username, String password, long time, SocketAddress requestor )
    {
        if( username.equals( this.username) && password.equals( this.password ) )
            {
                this.time = time;
                System.out.printf( "Client at %s has changed time to %d\n\n", requestor.toString(), time );
                return 0; // Time set correctly
            }
        else
            return 1; // Bad username/password
    }
    
}// End Server
