/**
 * Client.java
 * @author Alexander R. Cavaliere <arc6393@rit.edu>
 */

import java.net.InetSocketAddress;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.DatagramSocket;
import java.net.DatagramPacket;
import java.net.SocketException;

import java.io.IOException;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;

import java.util.Date;
import java.time.Instant;
import java.nio.charset.Charset;

public class Client
{

    // Class state
    private String host;
    private int port;

    private boolean useUDP;
    private boolean useCalendarFormat;

    private int queryTimes;
    private long time;

    private String username;
    private String password;

    private Socket tcpSocket;
    private DatagramSocket udpSocket; 
    
    private DataInputStream in;
    private DataOutputStream out;

    private ByteArrayInputStream bais;
    private ByteArrayOutputStream baos;
    
    private static final int MAXIMUM_MESSAGE_SIZE = 200;
    private static final int MAX_LENGTH_USERNAME_PASSWORD = 8;

    public Client( String host, int port, boolean useUDP, boolean useCalendarFormat, int queryTimes, int time,
                   String username, String password )
    {
        this.host = host;
        this.port = port;

        this.useUDP = useUDP;
        this.useCalendarFormat = useCalendarFormat;

        this.queryTimes = queryTimes;
        this.time = (long)time;

        this.username = username;
        this.password = password;

        try
            {
                if( useUDP )
                    {
                        udpSocket = new DatagramSocket();
                        System.out.printf( "Using UDP\n\n" );
                    }
                else
                    {
                        tcpSocket = new Socket();
                        tcpSocket.setTcpNoDelay( true ); // No sense in waiting around
                        tcpSocket.connect( new InetSocketAddress( host, port ) );

                        System.out.printf( "Using TCP\n\n" );
                    }
            }
        catch( SocketException soe )
            {
                System.out.println( soe );
                System.exit(1);
            }
        catch( IOException ioe )
            {
                System.out.println( ioe );
                System.exit(1);
            }
    }// End Client Constructor
    
    /**
     * setTime
     *
     * Sends a SET message to a Server using the Time Protocol! to change the Server's time.
     * You can find the specifics in the documentation. 
     */
    public void setTime()
    {
        // Make sure the username and password meet the protocol's length requirement
        if( username.length() > MAX_LENGTH_USERNAME_PASSWORD )
            username = username.substring( 0, MAX_LENGTH_USERNAME_PASSWORD );
        if( password.length() > MAX_LENGTH_USERNAME_PASSWORD )
            username = password.substring( 0, MAX_LENGTH_USERNAME_PASSWORD );

        byte b = 0;

        long roundTripTime = 0;
        long sendingTime = 0;
        long receivingTime = 0;

        long time = 0;
        
        byte[] incomingBuffer = null;
        DatagramPacket incomingPacket = null;
        ByteArrayOutputStream baos = null;
        DataOutputStream out = null;

        DataInputStream in = null;
        
        try
            {
                for( int round = 0; round < queryTimes; ++round )
                    {
                        if( useUDP )
                            {
                                incomingBuffer = new byte[MAXIMUM_MESSAGE_SIZE];
                                incomingPacket = new DatagramPacket( incomingBuffer, incomingBuffer.length ); 
                                
                                baos = new ByteArrayOutputStream();
                                out = new DataOutputStream( baos );
                            }
                        else
                            out = new DataOutputStream( tcpSocket.getOutputStream() );

                        // Write the message to change the time
                        out.writeByte( 'S' );
                        out.writeLong( this.time );
                        out.writeShort( username.length() );
                        out.writeBytes( username ); 
                        out.writeShort( password.length() );
                        out.writeBytes( password );
                        out.flush();
                        
                        sendingTime = System.currentTimeMillis();
                        
                        if( useUDP )
                            {
                                byte[] outBuffer = baos.toByteArray();
                                DatagramPacket packet = new DatagramPacket( outBuffer, outBuffer.length, new InetSocketAddress( host, port ) );
                                
                                udpSocket.send( packet );
                                sendingTime = System.currentTimeMillis();
                            }
                        
                        // Sent out the request and now we wait for a response!
                        if( useUDP )
                            {
                                udpSocket.receive( incomingPacket );
                                
                                receivingTime = System.currentTimeMillis();
                                
                                bais = new ByteArrayInputStream( incomingBuffer, 0, incomingPacket.getLength() );
                                in = new DataInputStream( bais );
                            }
                        else
                            in = new DataInputStream( tcpSocket.getInputStream() );
                        
                        b = in.readByte(); // TCP usage will block here
                        if( receivingTime == 0 )
                            receivingTime = System.currentTimeMillis();
                        printSendingAndReceivingTimes( sendingTime, receivingTime );
                        roundTripTime = receivingTime - sendingTime;
                        
                        
                        messageReceived( in, b, roundTripTime, true );

                    }// End For
            }
        catch( IOException ioe )
            {
                System.out.println( ioe );
                System.exit( 1 );
            }
    }

    /**
     * getTime
     *
     * Sends a message to a Server using the Time Protocol! which requests the Server's
     * time.
     */
    public void getTime()
    {
        byte b = 0; // message flag
        long roundTripTime = 0;
        long sendingTime = 0;
        long receivingTime = 0;
        long time = 0; // Server time
        byte[] incomingBuffer = null;
        DatagramPacket incomingPacket = null;
        ByteArrayOutputStream baos = null;
        DataOutputStream out = null;
        DataInputStream in = null;
        
        try
            {
                for( int round = 0; round < queryTimes; ++round )
                    {
                        if( useUDP )
                            {
                                incomingBuffer = new byte[MAXIMUM_MESSAGE_SIZE];
                                incomingPacket = new DatagramPacket( incomingBuffer, incomingBuffer.length ); 
                                
                                baos = new ByteArrayOutputStream();
                                out = new DataOutputStream( baos );
                            }
                        else
                            out = new DataOutputStream( tcpSocket.getOutputStream() );

                        // Write the Query Message
                        out.writeByte( 'Q' );
                        out.flush();
                        
                        sendingTime = System.currentTimeMillis();
                        
                        if( useUDP )
                            {
                                byte[] outBuffer = baos.toByteArray();
                                DatagramPacket packet = new DatagramPacket( outBuffer, outBuffer.length, new InetSocketAddress( host, port ) );
                                
                                        udpSocket.send( packet );
                                        sendingTime = System.currentTimeMillis();
                            }
                        
                        // Wait for the incoming message
                        if( useUDP )
                            {
                                udpSocket.receive( incomingPacket );
                                
                                receivingTime = System.currentTimeMillis();
                                
                                bais = new ByteArrayInputStream( incomingBuffer, 0, incomingPacket.getLength() );
                                in = new DataInputStream( bais );
                            }
                        else
                            in = new DataInputStream( tcpSocket.getInputStream() );
                        
                        b = in.readByte(); // TCP will just wait here
                        if( receivingTime == 0 )
                            receivingTime = System.currentTimeMillis();
                        printSendingAndReceivingTimes( sendingTime, receivingTime );
                        roundTripTime = receivingTime - sendingTime;
                        
                        messageReceived( in, b, roundTripTime, false );

                    }// End For
            }
        catch( IOException ioe )
            {
                System.out.println( ioe );
                System.exit( 1 );
            }
    }

    /**
     * printSendingAndReceivingTimes
     *
     * Helper message that prints out the sending and receiving times
     * @param sendingTime long The time a message was sent at
     * @param receivingTime long The time a response was gotten at
     */
    private void printSendingAndReceivingTimes( long sendingTime, long receivingTime )
    {
        System.out.printf( "Sent Query on %s\n\n", Instant.ofEpochMilli( sendingTime ).toString() );
        System.out.printf( "Received Response on %s\n\n", Instant.ofEpochMilli( receivingTime ).toString() );
    }

    /**
     * messageReceived
     *
     * Handles the output for the client. Displays in calendar format if the user wants it that way.
     * Otherwise the number of seconds from the epoch is displayed.
     * @param in DataInputStream The incoming reply from the server
     * @param b byte The message flag
     * @param roundTripTime long The round trip time
     * @param changeTime boolean If this message was a response to a Set message
     */
    private void messageReceived( DataInputStream in, byte b, long roundTripTime, boolean changeTime )
    {
        try
            {
                long time = 0;
                switch( b )
                    {
                    case 'T':
                        time = in.readLong();
                        if( changeTime )
                            System.out.printf( "Change Accepted\n\n" );
                        System.out.printf( "Round Trip Time: %d ms\n\n", roundTripTime );
                        
                        if( useCalendarFormat )
                            System.out.printf( "Server Time: %s\n\n", Instant.ofEpochSecond( time ).toString() );
                        else
                            System.out.printf( "Sever Time: %d \n\n", time );
                        break;
                        
                    case 'N':
                        time = in.readLong();
                        
                        System.out.printf( "Change Not Accepted. \n\n" );
                        System.out.printf( "Round Trip Time: %d ms\n\n", roundTripTime );
                        
                        if( useCalendarFormat )
                            System.out.printf( "Server Time: %s\n\n", Instant.ofEpochSecond( time ).toString() );
                        else
                            System.out.printf( "Sever Time: %d \n\n", time );
                        break;
                        
                    default:
                        System.err.println( "BAD MESSAGE" );
                        throw new IOException( "Bad Message Sent by Server" );    
                    }
            }
        catch( IOException ioe )
            {
                System.err.println( ioe );
                System.exit(1);
            }   
    }
}
