import java.io.IOException;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;

import java.nio.charset.Charset;
import java.util.Date;

/**
 * UDPServerReader
 *
 * Sits with the Timer Server and processes incoming UDP requests from
 * the Clients and sends them along to the Time Server.
 */
public class UDPServerReader extends Thread
{

    // Private Class State
    private DatagramSocket mailbox;
    private Server timeServer;

    // Upper limit on the read-in buffer
    private static final int MAXIMUM_MESSAGE_SIZE = 204;
    private static final int MAX_LENGTH_USERNAME_PASSWORD = 8;


    /**
     * UDPServerReader
     *
     * Public Constructor. Sets up the Reader so that it
     * can receive messages bound for the Pollster.
     */
    public UDPServerReader( DatagramSocket mailbox, Server timeServer )
    {
        this.mailbox = mailbox;
        this.timeServer = timeServer;
    }
    
    /**
     * run
     *
     * Processes incoming client messages regarding the time of the server (asking for or
     * setting it). 
     * Incoming Message Format(s);
     * query: Q [Byte]
     * set:   S [Byte] time [long] username_length [short] username [8byte ASCII_String] password_length [short] password [8byte ASCII_String] 
     * Outgoing Message Format(s):
     * time:                T [Byte] time [long]
     * negative ack change: N [Byte] time [long]
     */
    public void run()
    {
        byte[] buf = new byte [MAXIMUM_MESSAGE_SIZE];
        byte[] outBuf;

        DatagramPacket packet = new DatagramPacket (buf, buf.length);
        DatagramPacket responsePacket;

        ByteArrayInputStream bais;
        ByteArrayOutputStream baos;

        DataInputStream in;
        DataOutputStream out;

        byte b;

        String username = "";
        String password = "";

        int timeStatus;
        long time;

        try
            {
                for (;;)
                    {
                        mailbox.receive (packet);

                        System.out.printf( "Received UDP Packet from %s\n\n", packet.getSocketAddress().toString() );

                        bais = new ByteArrayInputStream( buf, 0, packet.getLength() );
                        in = new DataInputStream (bais);
                        b = in.readByte();

                        switch (b)
                            {
                            case 'Q': // Requesting the time of the server
                                baos = new ByteArrayOutputStream();
                                out = new DataOutputStream( baos );
                                out.writeByte( 'T' );
                                out.writeLong( timeServer.getTime() );
                                out.flush();

                                outBuf = baos.toByteArray();

                                responsePacket = new DatagramPacket( outBuf, outBuf.length, packet.getSocketAddress() );
                                mailbox.send( responsePacket );
                                
                                break;
                            case 'S': // Setting the new time of the server
                                time = in.readLong();

                                // Read in the username and password based on their respective lengths
                                short usernameLength = in.readShort();
                                for( short position = 0; position < usernameLength; ++position )
                                        username += (char)in.readByte();

                                short passwordLength = in.readShort();
                                for( short position = 0; position < passwordLength; ++position )
                                    password += (char)in.readByte();

                                timeStatus = timeServer.setTime( username, password, time, packet.getSocketAddress() );

                                // Clear the username and password for the next request
                                username = "";
                                password = "";
                                
                                baos = new ByteArrayOutputStream();
                                out = new DataOutputStream( baos );

                                if( timeStatus == 0 )
                                    out.writeByte( 'T' ); // ACK Time Change
                                else
                                    out.writeByte( 'N' ); // NAK Time Change
                                out.writeLong( timeServer.getTime() );
                                out.flush();
                                outBuf = baos.toByteArray();

                                responsePacket = new DatagramPacket( outBuf, outBuf.length, packet.getSocketAddress() );
                                mailbox.send( responsePacket );

                                break;
                            default:
                                System.err.println( "BAD MESSAGE" );
                                throw new IOException( "Bad Message Sent to Time Server" );
                            }
                    }// End For
            }
        catch (IOException ioe)
            {
                System.out.println( ioe );
                mailbox.close();
                System.exit(1);
            }
        finally
            {
                mailbox.close();
                System.exit(0);
            }
    }
}
